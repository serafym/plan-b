You will need JDK 1.8. 
Import as a Gradle project in Intellij.
Use build.xml to run Ant targets: first load and then run.
That's all.


For details about running and deploying see:

<http://www.moqui.org/framework/docs/RunDeploy.html>

Note that a runtime directory is required for Moqui Framework to run, but is not included in the source repository. The
Gradle get component, load, and run tasks will automatically add the default runtime (from the moqui-runtime repository).

For information about the current and near future status of Moqui Framework
see the [ReleaseNotes.md](https://github.com/moqui/moqui-framework/blob/master/ReleaseNotes.md) file.

For an overview of features see:

<http://www.moqui.org/framework/docs/features.html>

Get started with Moqui development quickly using the Tutorial at:

<http://www.moqui.org/framework/docs/Tutorial.html>

For comprehensive documentation of Moqui Framework and an overview of 
Mantle Business Artifacts download the PDF of the **Making Apps with Moqui** book:
 
<http://www.moqui.org/MakingAppsWithMoqui-1.0.pdf>
